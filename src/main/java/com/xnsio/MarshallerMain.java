package com.xnsio;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xnsio.filter.BucketCustomers;
import generated.CustomerRecords;
import generated.QueryResult;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static java.lang.Runtime.getRuntime;
import static java.lang.System.nanoTime;

public class MarshallerMain {

    public static final int ITERATIONS = 10_000;

    public static void main(String[] args) throws URISyntaxException, IOException, JAXBException, InterruptedException {

        execute();
    }

    public static List<CustomerRecords> execute() throws InterruptedException, JAXBException, URISyntaxException, JsonProcessingException {
        Runtime runtime = getRuntime();
        System.gc();
        Thread.sleep(2000);
        long initialMemoryUsed = memUsed(runtime);
        long cumulativeTime = 0l;
        for (int i = 0; i < ITERATIONS; i++) {
            long startTime = nanoTime();
            QueryResult result = buildInput();
            long timeElapsed = nanoTime() - startTime;
            cumulativeTime += timeElapsed;
            startTime = nanoTime();
            String jsonStr = output(new BucketCustomers().segregate(result));
            timeElapsed = nanoTime() - startTime;
            cumulativeTime += timeElapsed;
        }
        long afterMemoryUsed = memUsed(runtime);
        System.out.print("Mem consumed " + (afterMemoryUsed - initialMemoryUsed) / (1024 * 1024) + " MB");
        System.out.println(" & Exec-time: " + (cumulativeTime / 1000000) + " milli-secs");
        return null;
    }

    private static long memUsed(Runtime runtime) {
        return runtime.totalMemory() - runtime.freeMemory();
    }

    private static QueryResult buildInput() throws URISyntaxException, JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(QueryResult.class);

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        return (QueryResult) unmarshaller.unmarshal(MapperMain.class.getClassLoader().getResource("input.xml"));
    }

    private static String output(CustomerRecords rec) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(rec);
    }
}
