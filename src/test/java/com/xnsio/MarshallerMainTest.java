package com.xnsio;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;

class MarshallerMainTest {

    @Test
    void execute() throws InterruptedException, URISyntaxException, JsonProcessingException, JAXBException {
        MarshallerMain.execute();
    }
}